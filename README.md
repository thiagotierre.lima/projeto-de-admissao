
# Agenda de Contatos

Implementar uma agenda de contatos utilizando as tecnologias:
- Spring Boot
- Angular 2+
- PostgreSQL
- Hibernate

# Sobre o projeto

O projeto deve ser implementado utilizando as tecnologias citadas no tópico anterior. Implemente as entidades e relacionamentos presentes no Diagrama abaixo, buscando sempre utilizar as boas práticas de programação. Os dados devem ser persistidos em um banco de dados PostgreSQL, criado por você. Também deve ser implementada toda a estrutura visual (HTML, CSS, Javascript...). Ao final do projeto deve ser entregue uma aplicação em que o usuário possa realizar o login e acessar sua agenda, executando as operações básicas de Criação, Edição, Listagem e Remoção de Contatos. Espera-se que toda sua estrutura esteja conectada (Back-end, Front-end e o Banco de Dados) e funcionando.

**Obs:** O arquivo *insert_script.sql* deve ser usado para adicionar o usuário que realizará login no sistema.

# Regras de negócio

 - Cada usuário terá apenas uma agenda;
 - Não deve existir mais de um contato com o mesmo número de telefone;
 - Ao remover um contato, este deve ser apenas desativado, mantendo o seu registro no banco de dados.
 

 # Layout do projeto

 Fica como responsabilidade do(a) candidato(a) usar sua criatividade para criar um *layout* agradavél para a aplicação.

 # Requisitos obrigatórios

- O projeto deve conter no mínimo duas "páginas": A página inicial do `/login` e a tela de acesso à agenda `/agenda`;
- O projeto deve ser versionado usando GIT (Github ou Gitlab), em um repositório público. Este repositório deve conter, além do projeto, o *script* do banco de dados criado para inserir dados na aplicação, para que possamos realizar os testes no projeto;
- O link do repositório deve ser enviado para o e-mail *selecaogcti@parnamirim.rn.gov.br* com o título **PROJETO DE ADMISSÃO**.

 # *Implementações opcionais (Bônus)*

 - Implementar a busca de contatos por nome e/ou telefone;
 - Adicionar resposividade para celuluar ao Layout;
 - Usar API externa para buscar endereço pelo CEP;
 - Adicionar pagina para cadastro de usuário;
 - Adicionar o padrão de segurança **`BCryptPasswordEncoder`** para criptografia da senha de usuário no login;
 - Adicionar funcionalidade ***Esqueci minha senha*** e enviar uma nova senha pelo e-mail do usuário cadastrado no sistema;
 - Fica a critério do(a) candidato(a) implementar outras funcionalidades que não foram citadas.
 
# Diagrama

![Diagrama modelo](diagrama/diagrama.png)


